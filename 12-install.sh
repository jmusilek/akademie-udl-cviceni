#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
if [ ! "$SUDO_USER" ]; then
	control="$base_dir/.control.sh $base_name"
else
	control="sudo -u $SUDO_USER $base_dir/.control.sh $base_name"
fi
result="$base_dir/.result.sh $base_name"

STEPS=("install_cowsay" "install_from_ppa")


$control install_cowsay
if [ $? -ne 0 ]; then
    if ! dpkg --status cowsay &>/dev/null; then
        echo "1) Spusťte příkaz \"man cowsay\". Měli byste dostat chybovou hlášku."
        echo "2) Nainstalujte balík cowsay."
        echo "3) Znovu spusťte příkaz \"man cowsay\". Měli byste vidět manuálovou stránku."
        echo "4) Zadejte znovu příkaz: dvanacty"
        exit 1
    fi

    echo "Výborně, nainstalovali jste balík cowsay!"
    echo "TIP: Zkuste spustit příkaz: cowsay \"Dobré ráno!\""
    $control install_cowsay TRUE
fi


$control install_from_ppa
if [ $? -ne 0 ]; then
    if ! cat /etc/apt/sources.list /etc/apt/sources.list.d/* 2>/dev/null | grep '^[^#]' \
    | grep 'ubuntuhandbook1/stepmania' &>/dev/null; then
        echo "1) Zkuste vyhledat balík stepmania."
        echo "2) Přidejte repozitář ppa:ubuntuhandbook1/stepmania."
        echo "   sudo add-apt-repository ppa:ubuntuhandbook1/stepmania"
        echo "3) Znovu zkuste vyhledat balík stepmania."
        echo "4) Znovu spusťte příkaz: dvanacty"
        exit 1
    fi
    echo "Výborně, přidali jste repozitář ppa:ubuntuhandbook1/stepmania"

    if ! dpkg --status stepmania &>/dev/null; then
        echo "1) Nainstalujte balík stepmania."
        echo "2) Znovu spusťte příkaz: dvanacty"
        exit 1
    fi

    echo "Výborně, nainstalovali jste balík stepmania."
    $control install_from_ppa TRUE
fi


$result
