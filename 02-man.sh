#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("count" "argn" "arge")

# Count
$control count
if [ $? -ne 0 ]; then
	echo -e "Kolik má následující příkaz jednopísmenných přepínačů? echo -ne \"ahoj svete\""
  while true; do
    read count
    if [ $count -eq 2 ]; then
      echo "Správně!"
      $control count TRUE
      break
    else
      echo "Špatně, zkuste to prosím znovu:"
    fi
  done
fi

# argn
$control argn
if [ $? -ne 0 ]; then
	echo "Co dělá v programu echo přepínač -n ?"
	echo "1) Lépe zformátuje všechna čísla v zadaném řetězci (tzv. number-friendly)"
	echo "2) Nevypíše na konci řetězce nový řádek (smíchá se s novým promptem)"
	echo "3) Neinteraktivní mód programu echo"
	echo "Odpovězte jedním číslem"
  while true; do
    read argn
    if [ $argn -eq 2 ]; then
      echo "Správně!"
      $control argn TRUE
      break
    else
      echo "Špatně, zkuste to prosím znovu:"
    fi
  done
fi

# arge
$control arge
if [ $? -ne 0 ]; then
	echo "Co dělá v programu echo přepínač -e ?"
	echo "1) Povolí interpretaci speciálních významů zpětného lomítka"
	echo "2) Zakáže interpretaci speciálních významů zpětného lomítka"
	echo "3) Povolí rozšířenou funkcionalitu nových řádků (tzv. extended mód)"
	echo "Odpovězte jedním číslem"
  while true; do
    read arge
    if [ $arge -eq 1 ]; then
      echo "Správně!"
      $control arge TRUE
      break
    else
      echo "Špatně, zkuste to prosím znovu:"
    fi
  done
fi

$result
