#!/bin/bash

if ! which git &>/dev/null; then
    echo "Installing git"
    if ! which apt &>/dev/null; then
        echo "Používáte nepodporovaný systém! Nainstalujte si git sami."
        exit 1
    fi
    sudo apt install git
fi

INSTALL_DIR="$HOME/uvod-do-linuxu"
if [ ! -d "$INSTALL_DIR" ]; then
    mkdir $INSTALL_DIR
    if [ $? -ne 0 ]; then
        echo "Nedaří se vytvořit složku $INSTALL_DIR"
        exit 1
    fi
    echo "Stahuji cvičení"
    git clone https://gitlab.labs.nic.cz/jmusilek/akademie-udl-cviceni $INSTALL_DIR
else
    echo "Cvičení jsou již stažena, aktualizuji..."
    cd $INSTALL_DIR
    git pull
fi

cd $INSTALL_DIR
bash $INSTALL_DIR/check.sh install
