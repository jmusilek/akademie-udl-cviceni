#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("owner" "perm")

# Count
$control owner
if [ $? -ne 0 ]; then
	if [ "$(stat -c "%U" $base_dir/$base_name)" != "root" ]; then
		echo "Změnte vlastníka souboru $base_name na root"
		exit 1
	fi
	echo "Skvěle!"
	$control owner TRUE
fi

$control perm
if [ $? -ne 0 ]; then
	if [ "$(stat -c "%a" $base_dir/$base_name)" != "650" ]; then
		echo "Změnte práva souboru $base_name na rw-r-x---"
		exit 1
	fi
	echo "Skvěle!"
	$control perm TRUE
fi

$result
